#!/bin/bash

line="##########################################################################"
check_program(){ 
    $1
    if echo $? == 0; 
    then echo "$1 Everything Ok.";
    else
    echo "Error with program installation $1";
    fi
}

echo "# Setting up WSL - Ubuntu 20.04 Development Environment $(date)";
echo $line
 	sudo apt-get update 
	sudo apt-get upgrade
echo $line
echo "Installing PHP 7.4 $(date)";
	sudo apt-get install php7.4;
	sudo apt-get update;
echo $line

echo $line
echo "Installing Network Manager and tools $(date)";
	sudo apt-get install network-manager libnss3-tool jq xsel build-essential libssl-dev dnsmasq;
echo $line

echo $line 
echo "Install net-tools $(date)";
	sudo apt install net-tools;
echo $line

echo $line
echo "Installing NGINX $(date)";
	sudo apt-get install nginx;
echo $line


echo $line
echo "Installing MYSQL $(date)";
	sudo apt-get install mysql-server;
	sudo service mysql start;
echo $line



echo "Installing PHP Extensions $(date)";
	sudo apt-get install php-fpm php-cli php-mysql php-sqlite3 php-intl php-zip php-xml php-curl php-mbstring;

echo $line

echo "Installing Redis Server and PHP redis extension $(date)";
	sudo apt-get install redis-server php redis;
echo $line


echo "Running usermod on /var/lib/mysql mysql $(date)";
	sudo usermod -d /var/lib/mysql mysql;

echo $line

echo "Installing Composer $(date)";
	wget https://getcomposer.org/installer;
	php installer;
	sudo mv composer.phar /usr/local/bin/composer;
	check_program composer
echo $line

echo "Update the PATH environment variable";
	echo '#!/bin/bash' > $HOME/.bash_aliases;
	echo 'export PATH=$PATH:$HOME/.config/composer/vendor/bin' >> $HOME/.bash_aliases;
	echo 'export PATH=$PATH:$HOME/.local.bin' >> $HOME/.bash_aliases;
	echo ".config/composer/vendor/bin and .local.bin added to the PATH";
	source $HOME/.bashrc
echo $line	

echo "# Installing Laravel $(date)";
	composer global require "laravel/installer";
	echo "Laravel Installation Complete"
	check_program laravel
echo $line

echo "# Installing WP-CLI $(date)";
	curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar;
	check_program "wp-cli.phar --info"
	chmod +x wp-cli.phar;
	sudo mv wp-cli.phar /usr/local/bin/wp;
echo $line

echo "# Installing SVN version control $(date)";
	sudo apt-get update;
	sudo apt install subversion;
	echo "Installation Complete";
	check_program "svn --version";

echo $line

echo "# Installing Valet WSL $(date)";
	composer global require valeryan/valet-wsl;
	echo "Installation Complete";
	check_program "valet -v";
echo $line

echo $line
echo "# Installing NodeJs and NPM $(date)";
	sudo apt install nodejs;
	sudo apt install npm;
echo "Installation Complete";
	check_program "node -v";
	check_program "npm -v";
echo $line

echo $line
echo "# Installing Yarn $(date)";
	sudo npm install -g yarn;
	echo "Installation Complete"
	check_program "yarn -v"
echo $line

echo "Installing Vue and Vue-cli $(date)";
	npm install vue;
	echo "Vue Installation Complete";
	sudo npm i -g @vue/cli;
	echo "Vue-cli Installation Complete";
	check_program "vue -V"
echo $line

echo $line
echo "installing Vagrant $(date)";
	sudo apt install vagrant;
	echo "Vagrant Installed. Make sure you have VirtualBox installed if you plan to use vagrant.";
	check_program "vagrant -v"
echo $line


echo "## Python Environment setup ##";
echo "### Installing PIP $(date)";
	sudo apt install python3-pip;
	echo "PIP Installation Complete.";
	check_program "pip3 -V"
	
	echo "### Installing Requests and PSutil Modules $(date)";
	pip3 install requests psutil;
	echo "Installation complete";
	
	echo "### Instal Jupyter lab and notebook $(date)";
	pip3 install jupyterlab;
	pip3 install notebook;
	echo "Installation complete";
	echo "If setting up jupyper on WSL, you will need to install jupyterlab and notebook using windows power shell."
	echo "Type pip install jupyter and pip install notebook. Make sure Python is installed on Windows"
	echo "### Install Python scentific disctribution $(date)";
	pip3 install numpy scipy matplotlib ipython jupyter pandas sympy nose;
	echo "Installation complete modules: numpy scipy matplotlib ipython jupyter pandas sympy nose";

echo $line
echo "DONE!"
echo "
You can now run the following commands from the terminal:
php -h
laravel
composer
wp
node
npm
vue
yarn
valet
vagrant
service apache2 status | start | stop
service nginx status | start | stop
mysql -u root
redis-server
python3
pip3

"
