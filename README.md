# Setup Local Environment (Linux/WSL - UBUNTU 20.04 LTS)

Setup script for Linux Ubuntu 20.04 LTS development environment with multiple technologies.

My Development Environment 2020: https://fabiopacifici.com/my-development-environment-2020-ubuntu-20-04-lts/
My Development Environment 2018: https://fabiopacifici.com/my-development-environment-for-laravel-wordpress/

# Install the following Programs:

- Python:
  - PIP python3 package manager
  - Python Jupyther lab and jupiter notebook
  - Python modules: requests, psutil,
  - Python scientific distribution: numpy scipy matplotlib ipython jupyter pandas sympy nose
- PHP 7.4
- Composer
- MYSQL
- APACHE2
- NGINX
- Dns Masq and network tools
- PHP Extensions
- Redis Server and PHP redis Extension
- NodeJs
- Yarn
- Vue & Vue-cli (https://www.vuemastery.com/courses/real-world-vue-js/vue-cli/)
- Laravel
- WP-CLI
- Valet-linux
- Svn

# Run the script

./environment.sh

# Run the script and redirect errors

./environment.sh 2> errors.txt

# List of Available terminal commands

You can now run the following commands from the terminal:

php -h
laravel
composer
wp
svn
git
node
npm
vue
yarn
valet
vagrant
service apache2 status | start | stop
service nginx status | start | stop
mysql -u root
redis-server
python3
pip3
